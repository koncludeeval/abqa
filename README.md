# Absorption-Based Query Entailment Checking / Query Answering Evaluation Repository

This repository provides the evaluation framework and data for reproducing the results of the absorption-based query entailment checking / query answering experiments, which are part of the following publication:

- Andreas Steigmiller, Birte Glimm: Absorption-Based Query Answering for Expressive Description Logics; In Proceedings of the 18th International Semantic Web Conference (ISWC), October 26-30, 2019, Auckland, New Zealand.

An archived version of the evaluation framework and data (as used for the final paper) can be found at [https://zenodo.org/record/3266160](https://zenodo.org/record/3266160). This repository may contain some minor updates to improve the documentation and usability.

## Documentation

The accompanying [technical report](https://www.uni-ulm.de/fileadmin/website_uni_ulm/iui.inst.090/Publikationen/2019/StGl2019-ABQA-TR-ISWC.pdf "Technical Report") describes many details of the absorption-based query entailment checking / query answering (ABQA) technique and the integration in the reasoning system [Konclude](http://konclude.com "Konclude Website").


## Licences

This repository is a collection of tools, reasoners, and data (ontologies and queries), which are available as follows:

- `Konclude` (see http://konclude.con/ or https://github.com/konclude) available under the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.en.html)
- `OWL BGP` (see https://github.com/iliannakollia/owl-bgp) available under the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.en.html)
- `Pellet` (see https://github.com/stardog-union/pellet) available under an [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html)
  
- `Reattore HTTP Server` available under the [GPL](https://www.gnu.org/licenses/old-licenses/gpl-1.0.html) (see http://reattore.sourceforge.net/)

- Reactome ontology (https://reactome.org/) available under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
- UniProt ontology (https://www.uniprot.org/) available under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/)
- ChEMBL ontology (https://www.ebi.ac.uk/chembl/) available under [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/)
- Several other ontologies that are available without specifying licences, e.g., fly ontology (https://www.virtualflybrain.org/), OBI ontology (http://obi-ontology.org/), UOBM, LUBM, etc (see files in corresponding folders).

Optionally, you can also integrate the following (reasoning) system (via a scripted download):
- `PAGOdA` (see http://www.cs.ox.ac.uk/isg/tools/PAGOdA/) available under the [academic licence](http://www.cs.ox.ac.uk/isg/tools/PAGOdA/PAGOdA_Academic_Licence.txt)


## Sources

The source codes of all reasoners used in the evaluation is available in the `Sources.zip` file at [zenodo](https://zenodo.org/record/3266160). Note that the sources of `PAGOdA` and `Pellet` are unmodified, but we changed the build instructions for `OWL BGP` to make it directly compilable. In addition, we added a very simple SPARQL HTTP server wrapper to each reasoner such that they can uniformly be used in the evaluation.

The absorption-based query entailment checking / query answering approach is implemented in the included version of `Konclude` (v0.6.2-842), but it should be noted that some optimisations of `Konclude` are deactivated for this version since they are not (yet) fully compatible with the query answering integration. Also note that this version of `Konclude` is available under the version 3 of the Lesser General Public Licence (LGPLv3). For any use of the other reasoners, you also have to obey their licences, which are included in the corresponding folders.

For each reasoner, there exists a Linux bash script (`build.sh`/`build_release.sh`) that creates the binary for that reasoner (in a subfolder named `release`) as it is used by the evaluation framework. Note that pre-compiled versions of  `Pellet`, `OWL BGP`, and `Konclude` for Linux 64bit are already included in this repository (see the `Evaluation/Reasoners/REASONER_NAME/REASONER_VERSION/libs` directories). For `PAGOdA`, only the pre-compiled adapter is included and you may use the script `download_and_install_pagoda_under_academic_licence.sh` to download and install `PAGOdA`. Note that for any use of `PAGOdA` you have to comply with the [academic licence](http://www.cs.ox.ac.uk/isg/tools/PAGOdA/PAGOdA_Academic_Licence.txt) of `PAGOdA`. (The evaluation framework will try to evaluate `PAGOdA`, but will eventually skip to the next reasoner if `PAGOdA` is not available.) The other reasoners are available under the [LGPLv3](https://www.gnu.org/licenses/lgpl-3.0.en.html) (`OWL BGP`, `Konclude`) / the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) (`Pellet`) and, hence, can freely be used.

For compiling the reasoners from scratch, the following tools/libraries are required:

* JDK (required for `PAGOdA` and `OWL BGP`), JDK 8 (required for `Pellet`)
* Maven (required for `Pellet` and `PAGOdA`)
* Ant (required for `OWL BGP`)
* Antlr (required for `Pellet`)
* Docker (required for `Konclude`)

Note that `Konclude` can also be compiled without Docker, but this usually leads to a more system dependent binary. For nevertheless compiling `Konclude` manually, you can follow the commands in the Dockerfiles and/or check the `Konclude` readme.


## Evaluation

This repository contains the evaluation framework, compiled versions of the reasoners (as they are obtained from the build scripts in the `Sources.zip` archive from [zenodo](https://zenodo.org/record/3266160)), and all ontologies and queries from the experiments.

For the evaluations, we used `Konclude’s` integrated evaluator (i.e., the "Evaluator" binary in the `Evaluator` directory is also the binary of the reasoner `Konclude`). The evaluations can directly be executed on a Linux 64bit system (with an installed JRE for the other reasoners) by running the provided scripts (`run-entailment-checking-experiments.sh` and `run-query-answering-experiments.sh`). Note that the experiments approximately take 25 + 50 hours (depending on the hardware). The reasoners and the evaluator should not require more than 50 GB of RAM together. Also note that the port 8880 must be free since it is used by the reasoners as their SPARQL HTTP endpoint.

As an alternative to directly executing the evaluation, a Docker image can be created from the included Dockerfile (see script `create-evaluation-image.sh` in the `Docker` directory) that will run both experiments as soon as it is executed and, subsequently, it will start a webserver that exposes the results. Since the image is available via [Dockerhub](https://hub.docker.com/r/koncludeeval/abqa "https://hub.docker.com/r/koncludeeval/abqa"), the evaluation can also directly be started via a command line as follows:


```
docker run -p 8888:8888 --rm koncludeeval/abqa
```
For saving the results and analyses, you may run the container with mounted directories as follows:
```
docker run -p 8888:8888 --rm -v /path/to/save/results/Analyses:/data/Evaluation/Analyses -v /path/to/save/results/Responses:/data/Evaluation/Responses koncludeeval/abqa
``` 

## Results

All reasoner responses are saved into subdirectories of the `Evaluation/Responses` folder, but the evaluator is analysing all responses in the end and summarises some results in the `Evaluation/Analyses` directory. 
(If the evaluation framework is executed as a Docker container, then these responses and analyses can be found in subfolders of the directories that are mounted for `/data/Evaluation/Responses` and `/data/Evaluation/Analyses`).
An overview over the summarised results can be found in the HTML pages `Evaluation/Analyses/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html`
and `Evaluation/Analyses/A-000/Querying-SPARQL-KoncludeTests-Single-Fast001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html`, which should provide links to the most interesting charts and tables.

You may want to run the script `run-analyses-webserver.sh` to start a small web sever that exposes the `Evaluation/Analyses` directory on port [8888](http://localhost:8888 "http://localhost:8888") (which is automatically done for the Docker container of the evaluation framework after the experiments have completed) such that you can easily navigate through the summarised results.
The most interesting pages are probably as follows (assuming the web server has been started on the localhost):
* The [OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html) page in the `Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64` directory lists the most interesting charts/tables for the query answering experiment.
* The [complex-querying-accumulated-time-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/complex-querying-accumulated-time-table-vertical-chart.html) shows the accumulated query answering time for each reasoner over all or a specific group of queries/ontologies. For example, [complex-querying-accumulated-time-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/PAGOdATestQueries/Single/SpecialSelections/AbsoprtionBasedQueryAnswering/PAGOdATestOntologies/chembl/complex-querying-accumulated-time-table-vertical-chart.html) for chembl shows the accumulated query answering times for the chembl queries. Note that the preprocessing times are included for `Pellet` (since they cannot easily be separated) and to get the "actual query answering times" for `Pellet`, we subtracted the times for the simplest queries for the corresponding ontologies, which can be identified from the [time-mmm-separate-sorted-table-horizontal-chart](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/AccumulatedTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/PAGOdATestQueries/Single/SpecialSelections/AbsoprtionBasedQueryAnswering/PAGOdATestOntologies/chembl/complex-querying-accumulated-time-table-vertical-chart.html) pages for these ontologies. In fact, [time-mmm-separate-sorted-table-horizontal-chart](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/ComplexQueryingTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/time-mmm-separate-sorted-table-horizontal-chart.html) shows the query answering times (total processing time for the ResponseTimeComparison category) separately sorted for each reasoner, i.e., the smallest answering time (processing time) for each reasoner is shown on the left and the largest answering time (processing time) is shown on the right.
* The [result-difference-to-other-reasoners-sorted-descending-by-Average-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-PAGOdATests-Single-Fast001/Linux64/ResultDifferenceComparison/DirectoryGrouped/Q-000/Queries/SPARQL/result-difference-to-other-reasoners-sorted-descending-by-Average-table-vertical-chart.html) pages show the result differences with other reasoners, i.e., how many results of other reasoners are different by comparing the SPARQL binding results, but by ignoring the cardinality of each query answer.
* The [OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview](http://localhost:8888/A-000/Querying-SPARQL-KoncludeTests-Single-Fast001/Linux64/OnlyDirectoryGrouped-DirectlyLinkedNavigationOverview.html) page in the `Querying-SPARQL-KoncludeTests-Single-Fast001/Linux64` directory lists the most interesting charts/tables for the query entailment checking experiment.
* The [time-mmm-sorted-ascending-by-requests-table-vertical-chart](http://localhost:8888/A-000/Querying-SPARQL-KoncludeTests-Single-Fast001/Linux64/ComplexQueryingTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/KoncludeTestQueries/SpecialSelections/AbsorptionBasedQueryAnswering/KoncludeTestQueries/EntailmentTestingOntologies/time-mmm-sorted-ascending-by-Requests-table-vertical-chart.html) pages show the query entailment checking times for each query.
* Again, the [time-mmm-separate-sorted-table-horizontal-chart](http://localhost:8888/A-000/Querying-SPARQL-KoncludeTests-Single-Fast001/Linux64/ResponseTimeComparison/DirectoryGrouped/Q-000/Queries/SPARQL/KoncludeTestQueries/SpecialSelections/AbsorptionBasedQueryAnswering/KoncludeTestQueries/EntailmentTestingOntologies/time-mmm-separate-sorted-table-horizontal-chart.html) pages show the query entailment checking/total processing times for each reasoner separately sorted, which allow for easily extracting the minimal/maximal/average times for reasoners w.r.t. certain ontologies.
* The underlying data of most charts can also be download as CSV files (see links on the bottom of the pages).

Note that all reasoners are tested for the query entailment checking experiments, although these cyclic queries with only existential variables are not correctly/fully supported by all reasoners, i.e., the response times of other reasoners are not very meaningful. In fact, `PAGOdA` and `OWL BGP` do not support SPARQL ASK queries and, therefore, we converted them to SELECT queries and returned `yes` as soon as the first answer was found. Also note that the preprocessing times are included for `Pellet`, i.e., the actual query entailment checking and query answering times are better for `Pellet` as it seems.

All results are "cached" in files, i.e., if you stop and restart the evaluation, then it will continue where you stopped it. If you want to start the evaluation from scratch, then you have to delete the responses and analyses directories, e.g., by executing the `clean.sh` script (or by mounting new/different directories for the Docker container).

Our results (obtained from executing the experiments with Docker on a Dell PowerEdge R420 server with two Intel Xeon E5-2440 CPUs at 2.4 GHz and 144 GB RAM under a 64bit Ubuntu 16.04.5 LTS) are available at [zenodo](https://zenodo.org/record/3266160) (see `Results.zip` archive).