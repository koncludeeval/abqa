# Lines starting with '#' are interpreted as comments. The colors are only used if every reasoner matches only to one color assignment.

#FaCT++	#E4FECA
#HermiT	#BBD2FF
#Pellet	#404040
#Konclude	#3784A0
#Average	#F7C298
#Min	#D5803D
#Max	#603326