# Readme Update

This version of Pellet is extended by a simple http sparql wrapper 
(see package com.konclude.evaluation.pellet in the examples project). 
Since the original version from https://github.com/stardog-union/pellet
is released under the AGPL, this version is also available under
the AGPL.

For building pellet, you can use maven as usual (requires JDK 8):

```mvn install```

If the resulting `pellet-2.4.0-SNAPSHOT-dist.zip` file in the `distribution/target` 
folder is unzipped, it can be executed as follows:

```java -cp "lib/*" com.konclude.evaluation.pellet.SimpleHTTPSPARQLServerExecutor 8080```



# Original Readme


Pellet: An Open Source OWL DL reasoner for Java
-----------------------------------------------

[![Gitter](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/complexible/pellet?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)

Pellet is the OWL 2 DL reasoner: 
 
* [open source](https://github.com/complexible/pellet/blob/master/LICENSE.txt) (AGPL) or commercial license
* pure Java
* developed and [commercially supported](http://complexible.com/) by Complexible Inc. 

Pellet can be used with Jena or OWL-API libraries. Pellet provides functionality to check consistency of ontologies, compute the classification hierarchy, 
explain inferences, and answer SPARQL queries.

_Pellet 3.0, a closed source, next-gen version of Pellet, is embedded and available in [Stardog](http://stardog.com/), the RDF database._

Feel free to fork this repository and submit pull requests if you want to
see changes, new features, etc. in Pellet.

Documentation about how to use Pellet is in the doc/ directory and there are some 
code samples in the examples/ directory.                                    

Commercial support for Pellet is [available](http://complexible.com/). The [Pellet FAQ](http://clarkparsia.com/pellet/faq) answers some frequently asked questions.

There is a [pellet-users mailing list](https://groups.google.com/forum/?fromgroups#!forum/pellet-users) for questions and feedback. You can search [pellet-users archives](http://news.gmane.org/gmane.comp.web.pellet.user). 
Bug reports and enhancement requests should be sent to the mailing list. Issues are on [Github](http://github.com/complexible/pellet/issues).

Thanks for using Pellet.
