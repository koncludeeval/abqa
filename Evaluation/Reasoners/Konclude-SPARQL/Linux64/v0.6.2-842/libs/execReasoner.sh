#!/bin/bash
memlimit=`expr 31457280`
echo Memory Limit: $memlimit
ulimit -Sv $memlimit
./Evaluation/Reasoners/Konclude-SPARQL/Linux64/v0.6.2-842/libs/Konclude $*
trap 'kill $(jobs -p)' SIGINT SIGTERM EXIT