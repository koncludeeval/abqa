#!/bin/bash


./run-query-answering-experiments.sh 
./run-entailment-checking-experiments.sh

echo ""
echo "**********************************************************"
echo "*******          All evaluations finished          *******"
echo "**********************************************************"
echo ""
echo "Starting webserver for results, see http://localhost:8888/"

./run-analyses-webserver.sh